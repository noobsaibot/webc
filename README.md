Web Application Framework for C/C++
===================================

Description
-----------

Use C/C++ to rapidly develop web applications in a way only known to Ruby/Python based frameworks.
