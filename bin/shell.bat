@echo off

SET PROJECT_NAME=webc
SET MAIN_FILE=win32_webc.cpp
SET PROJECT_LINKER_FLAGS=user32.lib gdi32.lib winmm.lib Shlwapi.lib

IF exist %userprofile%\Projects (
    call %userprofile%\Projects\C\bin\global_shell.bat
)

IF exist U:\Projekte (
    call U:\Projekte\C\bin\global_shell.bat
)
