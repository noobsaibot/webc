#define _WINSOCKAPI_
#include <windows.h>

#include "webc.h"

internal_proc Param_Pair * lookup_mapping( Action_Context *context, char *key );
internal_proc Type_Field * lookup_field( Server_State *state, char *key );

#include "webc_view.cpp"

Platform_Api platform = {};

URL_MAPPING_METHOD(stub_handler) {
    printf( "%s\n", "<html><header><title>Stub</title><body><h1>Ganz tolle Wurst</h1></body></html>" );
}

Action_Handler default_handler = { "stub_handler", "stub_function", stub_handler };

Action_Handler * get_and_init_next_action_handler (Server_State *state, char *module_name,
        char *function_name, Url_Mapping_Method *run_function);
Module         * get_next_module                  (Server_State *state);
Type_Field     * get_next_field_definition        (Server_State *state);
View_Filter    * get_next_view_filter             (Server_State *state);

internal_proc Param_Pair create_char_param(char *key, char *value);

// module functions {{{
internal_proc Module *
load_module(Server_State *state, char* module_name, u64 timestamp) {
    for ( u32 module_count = 0; module_count < state->modules_count; ++module_count ) {
        Module *module = state->modules + module_count;

        if ( is_equal( module->name, module_name, string_len(module_name) ) ) {
            return module;
        }
    }

    size_t  str_size          = sizeof(char) * (string_len(module_name) + string_len(MODULE_DIR) + 2);
    char   *file_name         = (char *) _alloca( sizeof(char *) * str_size + 4 );
    char   *local_module_name = (char *) _alloca( sizeof(char *) * str_size );

    sprintf_s(local_module_name, str_size, "%s/%s", MODULE_DIR, module_name);
    sprintf_s(file_name, str_size + 4, "%s/%s.dll", MODULE_DIR, module_name);

    HMODULE win_module = LoadLibrary(local_module_name);

    if ( !win_module ) {
        // @TODO(serge): @diagnostics
        return NULL;
    }

    Module_Init_Method *init_function = (Module_Init_Method *) GetProcAddress(win_module, "init");
    Module_Config config = {};

    if ( init_function ) {
        config = init_function(state, &platform);
    }

    Module *module = get_next_module(state);

    module->name      = config.module_name;

    module->file_name = (char *) ALLOC_SIZE( &state->arena, string_len(file_name) * sizeof(char) );
    string_copy( file_name, module->file_name, string_len(file_name) );

    module->timestamp = timestamp;

    return module;
}

internal_proc void
preload_modules(char *path, Platform_Api *api, Server_State *state) {
    u32 result_count = 0;
    File_Find_Result *result = api->find_files_by_pattern(MODULE_DIR"/*.dll", &result_count);

    for ( u32 files_count = 0; files_count < result_count; ++files_count ) {
        File_Find_Result *file = result + files_count;

        load_module(state, file->file_name, file->last_written_timestamp);
    }
}

internal_proc Type_Field *
lookup_field( Server_State *state, char *key ) {
    Type_Field *result = NULL;
    auto key_length = string_len( key );

    for ( u32 field_index = 0; field_index < state->type_fields_count; ++field_index ) {
        Type_Field *field = state->type_fields + field_index;

        if ( string_len( field->name ) == key_length &&
             is_equal( field->name, key, key_length ) ) {

            result = field;
        }
    }

    return result;
}

internal_proc b32
lookup_route( Server_State *state, Action_Context *context,
        char **module_name, char **function_name ) {

    char *url = context->request->url;

    // @TODO(serge): save routes in a hash table for faster lookup
    for ( u32 route_index = 0; route_index < state->routes_count; ++route_index ) {
        Url_Route *route = state->routes + route_index;

        Lexer lex_pattern = {};
        Lexer lex_url     = {};

        lex_pattern.pos = route->url_pattern;
        lex_url.pos     = url;

        Token token_url = {};
        Token token_pattern = {};

        for (;;) {
            token_url     = get_token( &lex_url );
            token_pattern = get_token( &lex_pattern );

            if ( token_url.type == Token_EOS || token_pattern.type == Token_EOS ) {
                if ( token_pattern.type == Token_EOS ) {
                    *module_name   = route->module_name;
                    *function_name = route->function_name;

                    // @TODO(serge): populate context with found keys
                    return true;
                }
            }

            if ( token_url.type == token_pattern.type && token_url.type == Token_Ident ) {
                if ( !is_equal( token_url.text, token_pattern.text, token_url.length ) ) {
                    break;
                }
            }

            if ( token_url.type != token_pattern.type ) {
                if ( token_pattern.type != Token_Colon ) {
                    break;
                }

                token_pattern = get_token( &lex_pattern );

                // @TODO(serge): create param in context to store the token_url value
                // under token_pattern key
                auto key_value = create_char_param( token_pattern.text, token_url.text );
            }
        }
    }

    return false;
}

internal_proc Param_Pair *
lookup_mapping( Action_Context *context, char *key ) {
    Param_Pair *result = NULL;

    for ( u32 i = 0; i < context->view_mappings_count; ++i ) {
        result = context->view_mappings + i;
        if ( is_equal( key, result->key, string_len( key ) ) ) {
            break;
        } else {
            result = NULL;
        }
    }

    return result;
}

internal_proc Action_Handler *
lookup_handler( Server_State *state, char *module_name, char *function_name ) {
    // @TODO(serge): save handler in a hashtable for faster lookup
    for ( u32 handler_index = 0; handler_index < state->url_handler_count; ++handler_index ) {
        Action_Handler *handler = state->url_handler + handler_index;

        if ( handler &&
                is_equal(handler->module_name, module_name, string_len(handler->module_name)) &&
                is_equal(handler->function_name, function_name, string_len(function_name)) ) {

            return handler;
        }
    }

    return NULL;
}

inline void
extract_module_name( char *url, char *output ) {
    Lexer lex_name = {};
    lex_name.pos = url;
    lex_name.flags = LEXER_SKIP_WHITESPACE;

    eat_token( &lex_name );
    Token token = get_token( &lex_name );

    string_copy( token.text, output, token.length );
    output[token.length] = '\0';
}

internal_proc Action_Handler *
use_handler_if_exists(Platform_Api *api, Server_State *state,
        Action_Context *context, char *url) {

    Action_Handler *result = 0;

    char *postfix = "_module";
    auto postfix_len = string_len(postfix);

    char url_module_name[100];
    extract_module_name( url, url_module_name );
    string_append(postfix, url_module_name, string_len(postfix));

    load_module( state, url_module_name, 0 );

    char *function_name = "";
    char *module_name   = "";

    if ( lookup_route( state, context, &module_name, &function_name ) &&
            !is_empty( module_name ) && !is_empty( function_name ) ) {
        result = lookup_handler( state, module_name, function_name );
    }

    if ( !result ) {
        context->response->code = Response_Bad_Request;
        return &default_handler;
    }

    context->response->code = Response_Ok;

    return result;
}

inline void
dispatch(Platform_Api *api, Server_State *state, Action_Context *context) {

    Action_Handler *handler = use_handler_if_exists(api, state,
            context, context->request->url);

    ASSERT(handler != 0);
    handler->run_function(state, api, context);
}
// }}}
// helper functions {{{
Param_Pair*
find_param_by_name( Linked_List *list, char* key ) {
    u32 key_length = string_len( key );

    List_Entry *entry = next( list );
    while ( entry ) {
        Param_Pair *t = (Param_Pair *)entry->data;

        if ( is_equal( key, t->key, key_length ) ) {
            return t;
        }

        entry = next( list, entry );
    }

    return 0;
}

inline Linked_List*
param_list( Action_Context *context ) {
    return &context->request->params;
}

inline void
webc_init_server_state(Server_State *state) {
    state->arena        = {};

    state->routes       = (Url_Route *) ALLOC_SIZE( &state->arena, sizeof( Url_Route ) * 50 );
    state->url_handler  = (Action_Handler *) ALLOC_SIZE( &state->arena, sizeof( Action_Handler ) * 100 );
    state->modules      = (Module *) ALLOC_SIZE( &state->arena, sizeof( Module ) * 50 );
    state->type_fields  = (Type_Field *) ALLOC_SIZE( &state->arena, sizeof( Type_Field ) * 100 );
    state->view_filters = (View_Filter *) ALLOC_SIZE( &state->arena, sizeof( View_Filter ) * 50 );
}

Action_Handler *
get_and_init_next_action_handler(Server_State *state, char *module_name, char *function_name, Url_Mapping_Method *run_function) {
    Action_Handler *handler = state->url_handler + state->url_handler_count++;

    handler->module_name   = module_name;
    handler->function_name = function_name;
    handler->run_function  = run_function;

    return handler;
}

Module *
get_next_module(Server_State *state) {
    return (state->modules + state->modules_count++);
}

Type_Field *
get_next_field_definition( Server_State *state ) {
    return (state->type_fields + state->type_fields_count++);
}

View_Filter *
get_next_view_filter( Server_State *state ) {
    return (state->view_filters + state->view_filters_count++);
}

inline u32
safe_truncate_u64(s64 value) {
    ASSERT(value <= 0xFFFFFFFF);
    u32 result = (u32) value;

    return result;
}

internal_proc Param_Pair
create_custom_param(char *type_name, char *key, void *value) {
    Param_Pair result = {};

    result.key = key;
    result.custom_value = value;
    result.type = Param_Type_Custom;
    result.type_name = type_name;

    return result;
}

internal_proc Param_Pair
create_char_param(char *key, char *value) {
    Param_Pair result = {};

    result.key = key;
    result.char_value = value;
    result.type = Param_Type_Char;

    return result;
}

internal_proc Param_Pair
create_bool_param(char *key, bool value) {
    Param_Pair result = {};

    result.key = key;
    result.bool_value = value;
    result.type = Param_Type_Bool;

    return result;
}

internal_proc Param_Pair
create_int32_param(char *key, u32 value) {
    Param_Pair result = {};

    result.key = key;
    result.int32_value = value;
    result.type = Param_Type_Int;

    return result;
}

internal_proc Param_Pair
create_list_param(char *key, Linked_List* list) {
    Param_Pair result = {};

    result.key = key;
    result.char_value = (char *)list;
    result.type = Param_Type_Array;

    return result;
}

internal_proc Param_Pair
get_param(Param_Pair *params, u32 param_count, char *name) {
    for ( u32 param_index = 0; param_index < param_count; ++param_index ) {
        Param_Pair *param = params + param_index;
        if ( strcmp( param->key, name ) == 0 ) return *param;
    }

    return Param_Pair();
}

// }}}
// route functions {{{
Url_Route*
register_route(Server_State *state, char *url_pattern, char *module_name, char *function_name, Url_Mapping_Method *fun) {
    Url_Route *route = state->routes + state->routes_count;

    route->url_pattern   = url_pattern;
    route->module_name   = module_name;
    route->function_name = function_name;

    state->routes_count++;

    Action_Handler *handler = get_and_init_next_action_handler(state, module_name, function_name, fun);

    return route;
}
// }}}
// context functions {{{
internal_proc void
clear_context(Action_Context *context) {
    memset(context->view_mappings, 0, context->view_mappings_count * sizeof(Param_Pair));
    context->view_mappings_count = 0;
}
// }}}
// state functions {{{
Type_Field *
register_datafield_ext( Server_State *state, char *type_name, char *name, Param_Type type, u32 offset ) {
    Type_Field *result = get_next_field_definition( state );

    result->name      = name;
    result->type      = type;
    result->type_name = type_name;
    result->offset    = offset;

    return result;
}

View_Filter *
register_view_filter( Server_State *state, char *filter_name, View_Filter_Method *filter ) {
    View_Filter *result = get_next_view_filter( state );

    result->name = filter_name;
    result->filter = filter;

    return result;
}
// }}}
// register default filters {{{
DLL_EXPORT VIEW_FILTER_METHOD(uppercase) {

    char *result = (char *) malloc(sizeof(char) * strlen(value) + 1);
    char *tmp_result = result;

    while ( *value ) {
        if ( *value >= 'a' && *value <= 'z' ) {
            tmp_result[0] = ('A' + *value - 'a');
        } else {
            tmp_result[0] = *value;
        }

        value++;
        tmp_result++;
    }

    tmp_result[0] = 0;

    return result;
}
// }}}

