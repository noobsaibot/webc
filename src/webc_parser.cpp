#include <stdio.h>

#include "webc_parser.h"

internal_proc Ast_Node * parse_literal( Memory_Arena *arena, Parser *p, char *position, Ast_Node *parent );
internal_proc Ast_Node * parse_comment( Memory_Arena *arena, Parser *p, Ast_Node *parent );
internal_proc Ast_Node * parse_variable( Server_State *state, Action_Context *context, Parser *p, char *position, Ast_Node *parent );
internal_proc Ast_Node * parse_code( Action_Context *context, Parser *p, Ast_Node *parent );
internal_proc Ast_Node * parse_inherit( Memory_Arena *arena, Parser *p, char *position, Ast_Node *parent );
internal_proc Ast_Node * parse_foreach( Action_Context *context, Parser *p, char *position, Ast_Node *parent );

void
report_error( char *file_name, u64 row, u64 col, char *message ) {
    printf("ERROR: in %s line %llu:%llu : %s", file_name, row, col, message);
}

void
report_warning( char *file_name, u64 row, u64 col, char *message ) {
    printf("WARNING: in %s line %llu:%llu : %s", file_name, row, col, message);
}

void
report_info( char *file_name, u64 row, u64 col, char *message ) {
    printf("INFO: in %s line %llu:%llu : %s", file_name, row, col, message);
}

// @TODO(serge): give the parser a file stream to use in fprintf, which is a "compiled"
// template

internal_proc Ast_Node *
create_node( Memory_Arena *arena, Ast_Node *parent,
        Ast_Type type, char *value = "" ) {

    Ast_Node *node = ALLOC_STRUCT( arena, Ast_Node );
    node->entry    = ALLOC_STRUCT( arena, Ast_Entry );

    node->entry->type     = type;
    node->entry->value    = value;

    return node;
}

internal_proc void
parse_view( Server_State *state, Action_Context *context, char *content, char *file_name, char *response_data, u64 response_data_size ) {
    Lexer lex         = {};
    lex.pos           = content;
    Ast ast           = {};
    Token token       = {};
    Ast_Node *parent  = &ast.root;
    Ast_Node *node    = 0;
    Parser p          = { ast, file_name, &lex };

    while ( *lex.pos ) {

        token = get_token( &lex );
        /* literal value */
        if ( token.type != Token_OpenBrace && token.type != Token_EOS ) {
            node = parse_literal( &context->arena, &p, lex.pos - token.length, parent );
            string_append( node->entry->value, response_data, node->entry->length );
        }

        token = peek_token( &lex );
        /* {{ = variable */
        if ( token.type == Token_OpenBrace ) {
            eat_token( &lex );
            node = parse_variable( state, context, &p, lex.pos, parent );
            string_append( node->entry->value, response_data, node->entry->length );
        }

        /* {* = comment */
        else if ( token.type == Token_Asterisk ) {
            eat_token( &lex );
            node = parse_comment( &context->arena, &p, parent );
            string_append( node->entry->value, response_data, node->entry->length );
        }

        /* {# = code */
        else if ( token.type == Token_Hash ) {
            eat_token( &lex );
            node = parse_code( context, &p, parent );
            string_append( node->entry->value, response_data, node->entry->length );
        }

        token = get_token( &lex );
        if ( token.type != Token_CloseBrace ) {
            if ( token.type == Token_EOS ) break;
            report_error( file_name, lex.row, lex.col, "'}' expected\n" );
        }
    }
}

internal_proc Ast_Node *
parse_comment( Memory_Arena *arena, Parser *p, Ast_Node *parent ) {
    Lexer *lex = p->lexer;
    Ast_Node *node = create_node( arena, parent, Ast_Comment, lex->pos );

    while ( lex->pos && lex->pos[0] != '*' && lex->pos[1] && lex->pos[1] != '}' ) {
        skip( lex );
    }

    skip( lex );

    node->entry->length = lex->pos - node->entry->value;

    return node;
}

internal_proc Ast_Node *
parse_literal( Memory_Arena *arena, Parser *p, char *position, Ast_Node *parent ) {
    Lexer *lex = p->lexer;
    Ast_Node *node = create_node( arena, parent, Ast_Literal, position );

    Token token = {};
    for ( ;; )  {
        token = get_token( lex );

        if ( token.type == Token_EOS || token.type == Token_OpenBrace ) {
            break;
        }
    }

    node->entry->length = lex->pos - node->entry->value - token.length;

    return node;
}

internal_proc Ast_Expr *
parse_expression( Memory_Arena *arena, Parser *p, Ast_Expr *expr = NULL ) {
    Lexer *lex = p->lexer;
    auto flags = lex->flags;
    lex->flags |= LEXER_SKIP_WHITESPACE;

    Token t = get_token( lex );
    if ( t.type != Token_Ident ) {
        report_error( p->file_name, lex->row, lex->col, "identifier expected\n" );
    }

    if ( !expr ) {
        expr = ALLOC_STRUCT( arena, Ast_Expr );
    }

    char *value = (char *) ALLOC_SIZE( arena, sizeof( char ) * (t.length + 1) );
    string_copy( t.text, value, t.length );
    expr->node = create_node( arena, 0, Ast_Unknown, value );
    expr->type = Expr_Ident;

    t = peek_token( lex );
    if ( t.type == Token_Dot ) {
        eat_token( lex );
        expr->type = Expr_Binary;
        expr->child = ALLOC_STRUCT( arena, Ast_Expr );
        parse_expression( arena, p, expr->child );
    }

    lex->flags = flags;
    return expr;
}

internal_proc void
render_ident( Memory_Arena *arena, Param_Pair *pair, Ast_Node *node ) {
    // @TODO(serge): merge together with render_field
    switch ( pair->type ) {
        case Param_Type_Char: {
            auto length = string_len( pair->char_value );
            node->entry->value = (char *) ALLOC_SIZE( arena, sizeof( char ) * ( length + 1 ) );
            node->entry->length = length;
            string_copy( pair->char_value, node->entry->value, length );
        } break;

        case Param_Type_Utf: {
            printf("(TODO: print utf)");
        } break;

        case Param_Type_Int: {
            printf("(TODO: print int)");
        } break;
    }
}

internal_proc void
render_field( Memory_Arena *arena, Type_Field *field, void *field_ptr, Ast_Node *node ) {
    // @TODO(serge): merge together with render_ident
    switch ( field->type ) {
        case Param_Type_Char: {
            char *tmp_value = *(char **) field_ptr;
            auto tmp_len = string_len(tmp_value) + 1;
            node->entry->value = (char *) ALLOC_SIZE( arena, sizeof(char) * tmp_len );
            string_copy( tmp_value, node->entry->value, tmp_len );
        } break;

        case Param_Type_Utf: {
            wchar_t *field_value = *(wchar_t **) field_ptr;
            auto tmp_len = wcslen( field_value ) + 1;
            node->entry->value = (char *) ALLOC_SIZE( arena, sizeof(wchar_t) * tmp_len );
            size_t size;
            wcstombs_s( &size, node->entry->value, tmp_len, field_value, tmp_len );
        } break;

        case Param_Type_Int: {
            node->entry->value = (char *) ALLOC_SIZE( arena, sizeof(char) * 10 );
            int_to_str( *(int *) field_ptr, &node->entry->value );
        } break;
    }
}

internal_proc void
render_binary( Server_State *state, Memory_Arena *mem, Ast_Expr *expr,
        Param_Pair *pair, Ast_Node *node ) {

    if ( pair->type != Param_Type_Custom ) {
        report_error("<file_name>", 0, 0, "at the moment only custom arguments support dereference\n");
        return;
    }

    void *struct_ptr = ((u8 *) pair->custom_value);

    ASSERT( struct_ptr  != NULL );
    ASSERT( expr->child != NULL );

    Type_Field *field = lookup_field( state, expr->child->node->entry->value );

    if ( field == NULL ) return;

    if ( is_equal( field->type_name, pair->type_name, string_len( field->type_name ) ) ) {
        void *field_ptr = (((u8 *)struct_ptr) + field->offset);
        render_field(mem, field, field_ptr, node);
    }
}

internal_proc Ast_Node *
parse_variable( Server_State *state, Action_Context *context,
        Parser *p, char *position, Ast_Node *parent ) {

    Lexer *lex = p->lexer;

    char *value = NULL;
    Ast_Node *node = create_node( &context->arena, parent, Ast_Variable, position );
    Ast_Expr *expr = parse_expression( &context->arena, p );
    lex->flags |= LEXER_SKIP_WHITESPACE;
    Token token = {};

    if ( !expr ) {
        report_error( p->file_name, lex->row, lex->col, "invalid expression\n" );
    }

    Param_Pair *mapping = lookup_mapping( context, expr->node->entry->value );
    if ( expr->type == Expr_Ident ) {
        render_ident( &context->arena, mapping, node );
    } else if ( expr->type == Expr_Binary ) {
        render_binary( state, &context->arena, expr, mapping, node );
    }

    if ( peek_token( lex ).type == Token_Pipe ) {
        eat_token( lex );
        token = get_token( lex );

        if ( token.type != Token_Ident ) {
            report_error( p->file_name, lex->row, lex->col, "identifier expected\n" );
        }

        for ( u32 filter_index = 0; filter_index < state->view_filters_count; ++filter_index ) {
            auto *filter = state->view_filters + filter_index;

            if ( is_equal( token.text, filter->name, string_len( filter->name ) ) ) {
                value = filter->filter( state, value );
                break;
            }
        }
    }

    while ( (token = get_token( lex ) ).type != Token_CloseBrace ) {
    }

    return node;
}

internal_proc Ast_Node *
parse_code( Action_Context *context, Parser *p, Ast_Node *parent ) {
    Memory_Arena *arena = &context->arena;
    Ast_Node *node = 0;
    Lexer *lex = p->lexer;

    auto flags = lex->flags;

    lex->flags |= LEXER_SKIP_WHITESPACE;

    Token token = get_token( lex );

    if ( token.type != Token_Ident ) {
        report_error( p->file_name, lex->row, lex->col, "identifier expected\n" );
        return NULL;
    }

    if ( is_equal( token.text, "inherit", token.length ) ) {
        parse_inherit( arena, p, lex->pos, parent );
    } else if ( is_equal( token.text, "foreach", token.length ) ) {
        parse_foreach( context, p, lex->pos, parent );
    }

    while ( lex->pos[0] != '#' && lex->pos[1] != '}' ) {
        skip( lex );
    }

    skip( lex );
    lex->flags = flags;

    return node;
}

internal_proc Ast_Node *
parse_foreach( Action_Context *context, Parser *p, char *position, Ast_Node *parent ) {
    Memory_Arena *arena = &context->arena;

    Ast_Foreach *node = ALLOC_STRUCT( arena, Ast_Foreach );
    node->entry       = ALLOC_STRUCT( arena, Ast_Entry );

    node->entry->type     = Ast_Type_Foreach;

    init_list( &node->scope );

    Lexer *lex = p->lexer;

    Token token = get_token( lex );

    if ( token.type != Token_Ident ) {
        report_error( p->file_name, lex->row, lex->col, "identifier expected" );
    }

//    push_local( token.text );

    token = get_token( lex );
    if ( !is_equal( token.text, "in", token.length ) ) {
        report_error( p->file_name, lex->row, lex->col, "'in' expected" );
    }

    token = get_token( lex );
    if ( token.type != Token_Ident ) {
        report_error( p->file_name, lex->row, lex->col, "identifier expected" );
    }

    Param_Pair *mapping = lookup_mapping( context, token.text );
    ASSERT(mapping);

    Linked_List *list = (Linked_List *) mapping->char_value;

    return node;
}

internal_proc Ast_Node *
parse_inherit( Memory_Arena *arena, Parser *p, char *position, Ast_Node *parent ) {
    Ast_Node *node = create_node( arena, parent, Ast_Inherit, position );

    Lexer *lex = p->lexer;

    Token token = get_token( lex );

    if ( token.type != Token_QuotationMark ) {
        report_error( p->file_name, lex->row, lex->col, "'\"' expected\n" );
    }

    char file_name[1024];
    u32 pos = 0;
    for ( ;; ) {
        if ( lex->pos && lex->pos[0] != '"' ) {
            file_name[pos] = lex->pos[0];

            skip( lex );
            pos++;
            continue;
        }

        file_name[pos] = '\0';
        break;
    }

    skip( lex );

    return node;
}

internal_proc Ast_Node *
parse_foreach( Parser *p ) {
    Ast_Node *node = NULL;
    return node;
}

