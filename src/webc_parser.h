#ifndef WEBC_PARSER_H_WMX9SBGV
#define WEBC_PARSER_H_WMX9SBGV

struct Lexer;

enum Ast_Type {
    Ast_Unknown,
    Ast_Literal,
    Ast_Comment,
    Ast_Variable,
    Ast_Inherit,
    Ast_Type_Foreach,
};

enum Expr_Type {
    Expr_Binary,
    Expr_Ident,
};

struct Ast_Entry {
    Ast_Type type = Ast_Unknown;
    char*    value = "";
    u64      length = 0;
};

struct Ast_Node {
    Ast_Node*  parent = 0;
    Ast_Entry* entry  = 0;
};

struct Ast_Foreach : Ast_Node {
    Linked_List scope;
};

struct Ast_Expr {
    Expr_Type type;

    Ast_Node *node   = 0;
    Ast_Expr *child  = 0;
};

struct Ast {
    Ast_Node root;
};

struct Parser {
    Ast      ast;
    char*    file_name;
    Lexer*   lexer;
};

#endif /* end of include guard: WEBC_PARSER_H_WMX9SBGV */
