#include "webc.cpp"

#include "user_module.h"

#include <stdio.h>

#define MODULE_NAME "user"

DLL_EXPORT URL_MAPPING_METHOD(list) {
    User user = {};
    user.name = L"Serge Ratke";
    user.age  = 37;

    context->view_mappings = RESERVE_MAPPING_MEMORY( context, 2 );
    add_view_mapping(context, create_char_param("title", "Deine Mama"));
    add_view_mapping(context, create_custom_param("User", "user", &user));

    render_view( state, api, context, "user/list.html" );
}

DLL_EXPORT URL_MAPPING_METHOD(show) {
    User user = {};
    user.name = L"Serge Ratke";
    user.age  = 37;

    context->view_mappings = RESERVE_MAPPING_MEMORY( context, 2 );
    add_view_mapping(context, create_char_param("title", "User Show"));
    add_view_mapping(context, create_custom_param("User", "user", &user));

    render_view( state, api, context, "user/show.html" );
}

DLL_EXPORT MODULE_INIT_METHOD(init) {
    INIT_HEADER();

    Module_Config result = {};
    result.module_name = MODULE_NAME;

    auto age  = REGISTER_DATAFIELD( state, User, age,  Param_Type_Int );
    auto name = REGISTER_DATAFIELD( state, User, name, Param_Type_Utf );

    auto route_show_user = register_route( state, "/user/show/:name", "user_module", "show", show );
    auto route_list_user = register_route( state, "/user/list", "user_module", "list", list );

    return result;
}

