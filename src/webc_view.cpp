#include "webc_view.h"

#include "srr_lexer.cpp"
#include "webc_parser.cpp"

internal_proc void
add_view_mapping(Action_Context *context, Param_Pair param) {
    if ( !context ) return;

    ASSERT(context->view_mappings_count < MAX_VIEW_PARAM_COUNT);
    ASSERT(context->view_mappings_count < context->view_mappings_memory_reserved_count);

    Param_Pair *mapping = context->view_mappings + context->view_mappings_count;

    mapping->type      = param.type;
    mapping->type_name = param.type_name;
    mapping->key       = param.key;

    switch (param.type) {
        case Param_Type_Char: {
            mapping->char_value = param.char_value;
        } break;

        case Param_Type_Int: {
            mapping->int32_value = param.int32_value;
        }

        case Param_Type_Bool: {
            mapping->bool_value = param.bool_value;
        } break;

        case Param_Type_Custom: {
            mapping->custom_value = param.custom_value;
        } break;
    }

    context->view_mappings_count++;
}

internal_proc void
render_view( Server_State *state, Platform_Api *api, Action_Context *context, char* file_name ) {
    ASSERT(context);

    char *content;
    u64   content_size;

    u64 response_content_size = MB(10);
    context->response->content = (char *) ALLOC_SIZE( &context->arena, response_content_size );

    api->read_file( &context->arena, file_name, &content, &content_size );
    if ( content ) {
        parse_view( state, context, content, file_name, context->response->content, response_content_size );
    } else {
        context->response->code = Response_Not_Found;
    }
}

