#ifndef WEBC_TEMPLATE_H_GJ2L8OYG
#define WEBC_TEMPLATE_H_GJ2L8OYG

struct View_Template_Variable {
    char *name;
    u32   length;
    // value
};

enum View_Expression_Type {
    Expression_Loop,
    Expression_Condition,
};

struct View_Template_Expression {
    View_Expression_Type type;
};

#endif /* end of include guard: WEBC_TEMPLATE_H_GJ2L8OYG */
