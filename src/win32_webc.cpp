#include "webc.cpp"

#include <Shlwapi.h>

#include "lib/win_platform.cpp"

Win_State global_win_state;

// @TODO(serge): does this work with apache's cgi?
// @TODO(serge): compile time flag to set whether server operates standalone or in cgi mode

#if (WEBC_LOAD_MODULES == 1)
inline u64
win32_get_last_write_time( char *file_name ) {
    // @TODO(serge): work with module handles, which have to be saved first somewhere
    // GetModuleFileNameA()
    u64 result = {};

    GetModuleFileName(0, file_name, sizeof(file_name));

    if ( !PathFileExists( file_name ) ) {
        return result;
    }

    WIN32_FILE_ATTRIBUTE_DATA data;
    if ( GetFileAttributesEx( file_name, GetFileExInfoStandard, &data ) ) {
        result = (u64) data.ftLastWriteTime.dwHighDateTime << 32 | data.ftLastWriteTime.dwLowDateTime;
    }

    return result;
}
#endif

int
main(int argc, char **argv) {

    global_win_state.memory_sentinel.prev = &global_win_state.memory_sentinel;
    global_win_state.memory_sentinel.next = &global_win_state.memory_sentinel;

    platform.read_file             = win_read_file;
    platform.find_files_by_pattern = win_find_files_by_pattern;
    platform.allocate_memory       = win_allocate_memory;
    platform.free_memory           = win_free_memory;

    Server_State state = {};
    webc_init_server_state( &state );

    preload_modules(MODULE_DIR, &platform, &state);

    auto uppercase_filter = register_view_filter( &state, "uppercase", uppercase );

    Action_Context context = {};

    context.request  = ALLOC_STRUCT(&state.arena, Http_Request);
    context.response = ALLOC_STRUCT(&state.arena, Http_Response);

    init_list( &context.request->params );

    Http_Server server;
    server.ip = "127.0.0.1";
    server.port = 80;

    start_http_server( &server );

    while ( server.is_running ) {
        accept_request( context.request, &context.arena, server.socket );

#if (WEBC_LOAD_MODULES == 1)
        // @TODO(serge): observe modules directory for changes and load newly added modules, or reload
        // already loaded modules
        for ( u32 module_index = 0; module_index < state.modules_count; ++module_index ) {
            Module *module = state.modules + module_index;
            auto timestamp = win32_get_last_write_time( module->file_name );

            if ( module->timestamp != timestamp ) {
                load_module(&state, module->file_name, timestamp);
            }
        }
#endif

        dispatch(&platform, &state, &context);

        server_send_response( context.request, context.response, &context.arena, context.response->content );

        clear_context( &context );
    }

    stop_http_server( &server );

    return 0;
}
