#include "webc.cpp"

#include "content_module.h"

#define MODULE_NAME "content"

DLL_EXPORT URL_MAPPING_METHOD(list) {
    Content content = {};

    content.id   = 2;
    content.name = "foo_content_list";

    context->view_mappings = RESERVE_MAPPING_MEMORY( context, 2 );
    add_view_mapping(context, create_char_param("title", "Liste Inhalt auf"));
    add_view_mapping(context, create_custom_param("Content", "content", &content));

    render_view( state, api, context, "content/list.html" );
}

DLL_EXPORT URL_MAPPING_METHOD(show) {
    Content content = {};

    content.id   = 1;
    content.name = "foo_content_show";

    context->view_mappings = RESERVE_MAPPING_MEMORY( context, 2 );
    add_view_mapping(context, create_char_param("title", "Zeige Inhalt"));
    add_view_mapping(context, create_custom_param("Content", "content", &content));

    render_view( state, api, context, "content/show.html" );
}

DLL_EXPORT MODULE_INIT_METHOD(init) {
    INIT_HEADER();

    Module_Config result = {};
    result.module_name = MODULE_NAME;

    auto id   = REGISTER_DATAFIELD( state, Content, id,  Param_Type_Int );
    auto name = REGISTER_DATAFIELD( state, Content, name, Param_Type_Char );

    auto route_show_content = register_route( state, "/content/show/:id", MODULE_NAME, "show", show );
    auto route_list_content = register_route( state, "/content/list",     MODULE_NAME, "list", list );

    return result;
}


