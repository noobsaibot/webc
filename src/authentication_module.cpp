#include "authentication_module.h"

#include "webc.cpp"

#define MODULE_NAME "auth"

DLL_EXPORT URL_MAPPING_METHOD(show) {
    char* user_name     = "";
    char* user_password = "";

    context->view_mappings = RESERVE_MAPPING_MEMORY( context, 2 );

    add_view_mapping( context, create_char_param("user_name",     user_name));
    add_view_mapping( context, create_char_param("user_password", user_password));

    render_view( state, api, context, "authentication/show.html" );
}

DLL_EXPORT URL_MAPPING_METHOD(login) {
    Linked_List *list = param_list(context);

    Param_Pair *user_name = find_param_by_name( list, "user_name" );
    Param_Pair *user_pass = find_param_by_name( list, "user_password" );

    if ( is_equal( "serge", user_name->char_value ) && is_equal( "abcdef", user_pass->char_value ) ) {
        // erfolg
    } else {
        // fehlschlag
    }
}

DLL_EXPORT MODULE_INIT_METHOD(init) {
    INIT_HEADER();

    Module_Config result = {};
    result.module_name = MODULE_NAME;

    auto user_name     = REGISTER_DATAFIELD( state, Authentication_Data, user_name,     Param_Type_Char );
    auto user_password = REGISTER_DATAFIELD( state, Authentication_Data, user_password, Param_Type_Char );

    auto route_show_form = register_route( state, "/auth/show",  MODULE_NAME, "show", show );
    auto route_list_auth = register_route( state, "/auth/login", MODULE_NAME, "login", login );

    return result;
}

