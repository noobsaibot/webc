#ifndef WEBC_H_W9HONLRM
#define WEBC_H_W9HONLRM

#include <stdint.h>

#include "lib/common.cpp"
#include "lib/memory.cpp"
#include "lib/http_server.cpp"

// forward declarations {{{
struct Action_Context;
struct Param_Pair;
struct Action_Handler;
struct Memory_Arena;
struct Platform_Api;
struct Server_State;
struct File_Find_Result;
struct View_Filter;
struct Module_Config;
// }}}
// global defines {{{

#define MODULE_DIR "modules"

#define RESERVE_MAPPING_MEMORY( CONTEXT, COUNT, ... ) \
    ( Param_Pair * ) ALLOC_SIZE( &(CONTEXT)->arena, sizeof( Param_Pair ) * COUNT, ## __VA_ARGS__ ); \
    (CONTEXT)->view_mappings_memory_reserved_count = COUNT; \
    (CONTEXT)->view_mappings_count = 0;

#define REGISTER_DATAFIELD( STATE, TYPE_NAME, NAME, DATA_TYPE ) \
    register_datafield_ext( STATE, #TYPE_NAME, #NAME, DATA_TYPE, OFFSET_OF( TYPE_NAME, NAME ) )
// }}}
// config defines {{{
#define MAX_VIEW_PARAM_COUNT 10
// }}}
// proc defines {{{
#define URL_MAPPING_METHOD(NAME) void NAME( Server_State *state, Platform_Api *api, Action_Context *context )
typedef URL_MAPPING_METHOD(Url_Mapping_Method);

#define MODULE_INIT_METHOD(NAME) Module_Config NAME(Server_State *state, Platform_Api *api)
typedef MODULE_INIT_METHOD(Module_Init_Method);

#define INIT_HEADER()                                          \
    platform.allocate_memory       = api->allocate_memory;     \
    platform.free_memory           = api->free_memory;         \
    platform.read_file             = api->read_file;           \
    platform.find_files_by_pattern = api->find_files_by_pattern;

#define VIEW_FILTER_METHOD(NAME) char * NAME( Server_State *state, char *value )
typedef VIEW_FILTER_METHOD(View_Filter_Method);
// }}}
// data types {{{
struct Module_Config {
    char* module_name;
};

struct View_Filter {
    char* name;
    View_Filter_Method* filter;
};

struct Type_Field {
    Param_Type type;
    char*      name;
    char*      type_name;
    u32        offset;
};

struct Search_Result {
    u32 count;
    u32 size;
};

struct Action_Context {
    Memory_Arena   arena;

    u32            view_mappings_count;
    Param_Pair*    view_mappings;
    u32            view_mappings_memory_reserved_count;

    Http_Request*  request;
    Http_Response* response;
};

struct Action_Handler {
    char* module_name;
    char* function_name;

    Url_Mapping_Method* run_function;
};

struct Url_Route {
    char* url_pattern;
    char* module_name;
    char* function_name;
};

struct Module {
    char* name;
    char* file_name;
    u64   timestamp;
};

struct Server_State {
    Memory_Arena    arena;

    Url_Route*      routes;
    u32             routes_count;

    Action_Handler* url_handler;
    u32             url_handler_count;

    Module*         modules;
    u32             modules_count;

    Type_Field*     type_fields;
    u32             type_fields_count;

    View_Filter*    view_filters;
    u32             view_filters_count;
};
// }}}

#endif /* end of include guard: WEBC_H_W9HONLRM */

