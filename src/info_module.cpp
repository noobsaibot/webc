#include "webc.cpp"

#define MODULE_NAME "info"

DLL_EXPORT URL_MAPPING_METHOD(info) {
    Linked_List modules;
    init_list( &modules );

    for ( u32 i = 0; i < state->modules_count; ++i ) {
        Module *module = state->modules + i;

        char *name = (char *) ALLOC_SIZE( &context->arena, string_len( module->name ) * sizeof(char) );
        add_entry( &modules, name );
    }

    context->view_mappings = RESERVE_MAPPING_MEMORY( context, 3 );

    add_view_mapping( context, create_char_param("info", "webc") );
    add_view_mapping( context, create_list_param("modules", &modules) );
    add_view_mapping( context, create_char_param("version", "0.1") );

    render_view( state, api, context, "info/show.html" );
}

DLL_EXPORT MODULE_INIT_METHOD(init) {
    INIT_HEADER();

    Module_Config result = {};
    result.module_name = MODULE_NAME;

    auto route_show_form = register_route( state, "/info",  MODULE_NAME, "info", info );

    return result;
}

