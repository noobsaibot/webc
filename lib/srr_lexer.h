enum Lexer_Flags {
    LEXER_NONE            = 0x0,
    LEXER_SKIP_WHITESPACE = 0x1,
};

enum Token_Type {
    Token_Unknown,

    Token_Ampersand,
    Token_Apostrophe,
    Token_Asterisk,
    Token_Backslash,
    Token_Circumflex,
    Token_CloseBrace,
    Token_CloseBracket,
    Token_CloseParen,
    Token_Colon,
    Token_Coma,
    Token_Dollar,
    Token_Dot,
    Token_Equals,
    Token_Euro,
    Token_ExclamationMark,
    Token_Greater,
    Token_Hash,
    Token_Minus,
    Token_Newline,
    Token_OpenBrace,
    Token_OpenBracket,
    Token_OpenParen,
    Token_Paragraph,
    Token_Percent,
    Token_Pipe,
    Token_Plus,
    Token_QuestionMark,
    Token_QuotationMark,
    Token_Semicolon,
    Token_Slash,
    Token_Smaller,
    Token_Space,
    Token_Tab,
    Token_Underscore,

    Token_Ident,
    Token_Number,

    Token_EOS,
};

struct Token {
    Token_Type type;
    u64        length;
    char*      text;
};

struct Lexer {
    char* pos;
    u64   row = 1;
    u64   col = 1;
    u32   flags;
};

internal_proc Token get_token(Lexer *lex);
internal_proc b32 is_eol(char c);
internal_proc u32 eat_whitespace(Lexer *lex);
internal_proc b32 is_alpha(char c);
internal_proc b32 is_numeric(char c);
internal_proc b32 require_token( Lexer *lex, Token_Type desired_type );
internal_proc void eat_token(Lexer *lex);
