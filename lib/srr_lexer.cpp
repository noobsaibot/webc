#include "srr_lexer.h"

internal_proc void
skip( Lexer *lex, s64 step = 1 ) {
    lex->pos += step;
    lex->col += step;
}

internal_proc b32
is_eol(char c) {
    return (c == '\n' || c == '\r');
}

internal_proc b32
is_equal( Token *a, Token *b ) {
    if ( a->type != b->type )     return false;
    if ( a->length != b->length ) return false;

    for ( u32 i = 0; i < a->length; ++i ) {
        if ( a->text[i] != b->text[i] ) return false;
    }

    return true;
}

internal_proc Token
peek_token( Lexer *lex ) {
    Token token  = get_token( lex );
    skip( lex, 0 - token.length );

    return token;
}

internal_proc u32
eat_whitespace(Lexer *lex) {
    u32 count = 0;
    while ( lex->pos[0] == ' ' || lex->pos[0] == '\t' || is_eol( lex->pos[0] ) ) {
        if ( is_eol( lex->pos[0] ) ) {
            lex->row++;
            lex->col = 1;
        }

        skip( lex );
        count++;
    }

    return count;
}

internal_proc void
eat_token( Lexer *lex ) {
    get_token( lex );
}

internal_proc b32
is_alpha(char c) {
    return ( c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' );
}

internal_proc b32
is_numeric(char c) {
    return ( c >= '0' && c <= '9' );
}

internal_proc b32
expect_token( Lexer *lex, Token_Type desired_type ) {
    Token token = get_token( lex );
    b32 result = (token.type == desired_type);

    return result;
}

internal_proc Token
get_token(Lexer *lex) {

    if ( lex->flags & LEXER_SKIP_WHITESPACE ) {
        u32 skipped_chars = eat_whitespace( lex );
    }

    Token token = { Token_Unknown, 1, lex->pos };

    char c = lex->pos[0];
    skip( lex );

    switch ( c ) {
        case '&': {
            token.type = Token_Ampersand;
        } break;
        case '\'': {
            token.type = Token_Apostrophe;
        } break;
        case '*': {
            token.type = Token_Asterisk;
        } break;
        case '\\': {
            token.type = Token_Backslash;
        } break;
        case '^': {
            token.type = Token_Circumflex;
        } break;
        case '}': {
            token.type = Token_CloseBrace;
        } break;
        case ']': {
            token.type = Token_CloseBracket;
        } break;
        case ')': {
            token.type = Token_CloseParen;
        } break;
        case ':': {
            token.type = Token_Colon;
        } break;
        case ',': {
            token.type = Token_Coma;
        } break;
        case '$': {
            token.type = Token_Dollar;
        } break;
        case '.': {
            token.type = Token_Dot;
        } break;
        case '=': {
            token.type = Token_Equals;
        } break;
        case '€': {
            token.type = Token_Euro;
        }
        case '!': {
            token.type = Token_ExclamationMark;
        } break;
        case '>': {
            token.type = Token_Greater;
        } break;
        case '#': {
            token.type = Token_Hash;
        } break;
        case '-': {
            token.type = Token_Minus;
        } break;
        case '\n': {
            token.type  = Token_Newline;
            lex->row   += 1;
            lex->col    = 1;
        } break;
        case '\r':
        case '\v': {
            token.type  = Token_Newline;
        } break;
        case '{': {
            token.type = Token_OpenBrace;
        } break;
        case '[': {
            token.type = Token_OpenBracket;
        } break;
        case '(': {
            token.type = Token_OpenParen;
        } break;
        case '§': {
            token.type = Token_Paragraph;
        } break;
        case '%': {
            token.type = Token_Percent;
        } break;
        case '|': {
            token.type = Token_Pipe;
        } break;
        case '+': {
            token.type = Token_Plus;
        } break;
        case '"': {
            token.type = Token_QuotationMark;
        } break;
        case '?': {
            token.type = Token_QuestionMark;
        } break;
        case ';': {
            token.type = Token_Semicolon;
        } break;
        case '/': {
            token.type = Token_Slash;
        } break;
        case '<': {
            token.type = Token_Smaller;
        } break;
        case ' ': {
            token.type = Token_Space;
        } break;
        case '\t': {
            token.type = Token_Tab;
        } break;
        case '_': {
            token.type = Token_Underscore;
        } break;
        case '\0': {
            token.type = Token_EOS;
        } break;
        default: {
            /* identifier */
            if ( is_alpha( c ) ) {
                token.type = Token_Ident;

                while ( is_numeric( lex->pos[0] ) || is_alpha( lex->pos[0] ) ||
                        lex->pos[0] == '_' ) {
                    skip( lex );
                }

                token.length = lex->pos - token.text;
            }

            /* number */
            else if ( is_numeric( c ) ) {
                token.type = Token_Number;

                while ( is_numeric( lex->pos[0] ) || lex->pos[0] == '_' ||
                        ( lex->pos[0] == '.' && lex->pos[1] && is_numeric(lex->pos[1]) ) ) {
                    skip( lex );
                }

                token.length = lex->pos - token.text;
            }
        } break;
    }

    return token;
}

